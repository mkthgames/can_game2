﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour {

    public static GameManager _instance;
    public GameObject topLight;
    public GameObject middleLight;
    public GameObject bottomLight;
    public GameObject centerLight;
    public Camera cam;

    public Text gameOverScoreTxt;

    public List<AudioClip> AudioSounds;
    public AudioSource AudioPlayer;

    public float minDistance = 0.02f;

    void OnEnable()
    {
        _instance = this;
    }

    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Vector2 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            
            //RaycastHit2D hit = Physics2D.Raycast(mousePos, Vector2.zero);
            
            GameObject[] cans = GameObject.FindGameObjectsWithTag("can");
            bool hitAnything = false;
            foreach(GameObject can in cans)
            {
                if(Vector2.Distance(can.transform.position, mousePos) < minDistance)
                {
                    can.transform.GetComponent<CanBehavior>().CanPressed();
                    PlaySound("canPressed");
                    hitAnything = true;
                    return;
                }
            }
            if(!hitAnything)
                PlaySound("canPressedMissed");

            /*
            //if anything is collided
            if (hit.collider != null)
            {
                if(hit.collider.tag == "can")
                {
                    hit.collider.transform.GetComponent<CanBehavior>().CanPressed();
                    PlaySound("canPressed");
                }
            }

            if (hit.collider == null || (hit.collider != null && hit.collider.tag != "can"))
            {
                PlaySound("canPressedMissed");
            }
            */
        }
    }

    public void ActivateLight(int nr)
    {
        switch (nr)
        {
            case 0:
                topLight.SetActive(false);
                topLight.SetActive(true);
                break;
            case 1:
                middleLight.SetActive(false);
                middleLight.SetActive(true);
                break;
            case 2:
                bottomLight.SetActive(false);
                bottomLight.SetActive(true);
                break;
            case 3:
                centerLight.SetActive(false);
                centerLight.SetActive(true);
                break;
        }
    }

    public void PlaySound(string name)
    {
        if (!Constants.cons.isSoundActive)
            return;

        switch (name)
        {
            case "canPressed":
                AudioPlayer.PlayOneShot(AudioSounds[0]);
                break;
            case "canPressedMissed":
                AudioPlayer.PlayOneShot(AudioSounds[1]);
                break;
            case "canCollected":
                AudioPlayer.PlayOneShot(AudioSounds[2]);
                break;
            case "canLost":
                AudioPlayer.PlayOneShot(AudioSounds[3]);
                break;
        }

    }
   
}
