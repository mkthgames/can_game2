﻿using UnityEngine;
using System.Collections;

public class ClickEvents : MonoBehaviour {
	public static ClickEvents sharedClickEvents;
	float timeDurationForTween = 0.3f;
    public AudioSource MusicPlayer;

	void Awake(){
		sharedClickEvents = this;
	}
	void prepareForNewGame(){
		Constants.cons.isGameStarted = true;
		Constants.cons.currentRound = 0;
		Constants.cons.score = 0;
		Constants.cons.isGameOver = false;
		KeepCreatingObjects.sharedInstance.startNewRound ();

	}
	public void btnPlayPressed(){
//		ObjHolders.sharedObjects.UIMainGame.GetComponent<UIStretch> ().runOnlyOnce = true;
//		ObjHolders.sharedObjects.UIMainMenu.GetComponent<UIStretch> ().runOnlyOnce = true;
//		TweenScale.Begin (ObjHolders.sharedObjects.UIMainMenu,timeDurationForTween,Vector3.zero);
//		TweenScale.Begin (ObjHolders.sharedObjects.UIMainGame,timeDurationForTween,Vector3.one);
		ObjHolders.sharedObjects.UIMainMenu.SetActive (false);
		ObjHolders.sharedObjects.UIMainGame.SetActive (true);
		EventManager.FireOnStartGame ();
		prepareForNewGame ();

	}
	public void btnHelpPressed(){
//		TweenScale.Begin (ObjHolders.sharedObjects.UIHelpScreen, timeDurationForTween, Vector3.one);
//		TweenScale.Begin (ObjHolders.sharedObjects.UIMainMenu,timeDurationForTween,Vector3.zero);
		ObjHolders.sharedObjects.UIHelpScreen.SetActive (true);
		ObjHolders.sharedObjects.UIMainMenu.SetActive (false);
	}

	public void btnSoundOnClicked(){
        MusicPlayer.Pause();
        Constants.cons.isSoundActive = false;
		ObjHolders.sharedObjects.btnSoundOn.SetActive (false);
		ObjHolders.sharedObjects.btnSoundOff.SetActive (true);
	}
	public void btnSoundOffClicked()
    {
        MusicPlayer.Play();
        Constants.cons.isSoundActive = true;
        ObjHolders.sharedObjects.btnSoundOn.SetActive (true);
		ObjHolders.sharedObjects.btnSoundOff.SetActive (false);
	}
	public void btnCloseMainGameClicked(){
//		TweenScale.Begin (ObjHolders.sharedObjects.UIMainMenu,timeDurationForTween,Vector3.one);
//		TweenScale.Begin (ObjHolders.sharedObjects.UIMainGame,timeDurationForTween,Vector3.zero);
		ObjHolders.sharedObjects.UIMainMenu.SetActive (true);
		ObjHolders.sharedObjects.UIMainGame.SetActive (false);
		Constants.cons.isGameStarted = false;
		KeepCreatingObjects.sharedInstance.processForHighScore ();
	}

	//       Help Screnn Buttons
	public void btnCloseHelpClicked(){
//		TweenScale.Begin (ObjHolders.sharedObjects.UIHelpScreen,timeDurationForTween,Vector3.zero);
//		TweenScale.Begin (ObjHolders.sharedObjects.UIMainMenu,timeDurationForTween,Vector3.one);
		ObjHolders.sharedObjects.UIHelpScreen.SetActive (false);
		ObjHolders.sharedObjects.UIMainMenu.SetActive (true);
	}

	//Game Over Buttons 
	public void btnCloseGameOverClicked(){
//		TweenScale.Begin (ObjHolders.sharedObjects.UIGameOver,timeDurationForTween,Vector3.zero);
//		TweenScale.Begin (ObjHolders.sharedObjects.UIMainMenu,timeDurationForTween,Vector3.one);
		ObjHolders.sharedObjects.UIGameOver.SetActive (false);
		ObjHolders.sharedObjects.UIMainMenu.SetActive (true);
	}

	public void showGameOverScreen(){
//		TweenScale.Begin (ObjHolders.sharedObjects.UIMainGame,timeDurationForTween,Vector3.zero);
//		TweenScale.Begin (ObjHolders.sharedObjects.UIGameOver, timeDurationForTween, Vector3.one);
		ObjHolders.sharedObjects.UIGameOver.SetActive (true);
		ObjHolders.sharedObjects.UIMainGame.SetActive (false);
	}
	public void btnPlayAgainPressed(){
//		TweenScale.Begin (ObjHolders.sharedObjects.UIGameOver,timeDurationForTween,Vector3.zero);
//		TweenScale.Begin (ObjHolders.sharedObjects.UIMainGame, timeDurationForTween, Vector3.one);
		ObjHolders.sharedObjects.UIMainGame.SetActive (true);
		ObjHolders.sharedObjects.UIGameOver.SetActive (false);
		prepareForNewGame ();
	}

	public void btnPausePressed(){
		Constants.cons.isGamePaused = !Constants.cons.isGamePaused;
		//EventManager.FireOnPauseGame ();
	}
}
