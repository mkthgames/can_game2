﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
public class KeepCreatingObjects : MonoBehaviour {

	GameObject cans;
	GameObject sparkEffect;
	float[] yPosvalue = { 0.4f,0.45f,0.5f,0.55f};
	// Use this for initialization
	bool isClicked = false;
	int numberOfCansCreated = 0;

    private float cansTimer = 0;
    private float cansTimerTarget = 2;

    private float roundTimer = 0;
    private float roundTimerTarget = 8;
    List<float> rnd = new List<float>();

    public static KeepCreatingObjects sharedInstance;
	void Awake(){
		sharedInstance = this;
	}
	void OnEnable(){
		EventManager.OnStartGame += StarttheGame ;
	}
	void OnDisable(){
		EventManager.OnStartGame -= StarttheGame ;
	}

	void StarttheGame () {

        rnd.Add(-0.7f);
        rnd.Add(-0.2f);
        rnd.Add(0.5f);
        cans = Resources.Load ("Prefabs/Cans") as GameObject;	
		sparkEffect = Resources.Load ("Prefabs/S1") as GameObject;
	//	InvokeRepeating ("generateObjects",5f,2f);
		isClicked = false;
        //Constants.cons.currentHighScore = PlayerPrefs.GetInt("HighScore", 0);
        ObjHolders.sharedObjects.lblHighScore.text = PlayerPrefs.GetInt("HighScore", 0).ToString();
        Constants.cons.currentHighScore = PlayerPrefs.GetInt("HighScore", 0);

        //PlayerPrefs.DeleteAll();
        /*
		if (PlayerPrefs.GetInt ("HighScore",0) != 0) {
			Debug.Log("Setting HighScore " + PlayerPrefs.GetInt (Constants.cons.kHighscore).ToString() );
			if(ObjHolders.sharedObjects != null)
				ObjHolders.sharedObjects.lblHighScore.text = PlayerPrefs.GetInt (Constants.cons.kHighscore).ToString();
			Constants.cons.currentHighScore = PlayerPrefs.GetInt (Constants.cons.kHighscore);
		}
		else{
			Debug.LogError("Setting 0 HighScore");

			Constants.cons.currentHighScore = 0;
			ObjHolders.sharedObjects.lblHighScore.text =  Constants.cons.currentHighScore.ToString();
			PlayerPrefs.SetInt (Constants.cons.kHighscore, Constants.cons.currentHighScore);
		}
        */
    }
    

	void generateObjects(){

		if (! Constants.cons.isGameStarted || !Constants.cons.isNewRoundStarted || Constants.cons.isGameOver || Constants.cons.isGamePaused )
			return;

		//int rnd = Random.Range (0, (yPosvalue.Length));
		//float rnd = Random.Range(-0.6f, 0.2f);
        int random = Random.Range(0, 3);
        Vector3 randomVector = new Vector3 (2.0f, rnd[random], 0.0f);
       // Debug.Log(rnd);
		Instantiate (cans, randomVector, Quaternion.identity);
		numberOfCansCreated++;

		/* Round wise Game Play logic
		if (numberOfCansCreated % 5 == 0 ) {
			startNewRound();
			Constants.cons.isNewRoundStarted = false;
		}
		*/
	}
	public void processForHighScore(){
		if(checkForHighScore(Constants.cons.score)){
            PlayerPrefs.SetInt("HighScore", Constants.cons.score);
			ObjHolders.sharedObjects.lblHighScore.text = Constants.cons.score.ToString();
		}
	}
	public bool checkForHighScore(int score){
		if (Constants.cons.currentHighScore > score) {
			return false;
		} else 
			return true;
	}
    // Update is called once per frame
    void Update()
    {
        if (Constants.cons.isGamePaused || !Constants.cons.isGameStarted || Constants.cons.isGameOver)
            return;

        cansTimer += Time.deltaTime;
        int round = Mathf.Clamp(Constants.cons.currentRound, 0, 15);
       
        if (cansTimer > cansTimerTarget - round / 10)
        {
            generateObjects();
            cansTimer = 0;
        }

        roundTimer += Time.deltaTime;
        if (roundTimer > roundTimerTarget)
        {
            Constants.cons.currentRound++;
            ObjHolders.sharedObjects.lblRound.GetComponent<Text>().text = "Round : " + Constants.cons.currentRound;
            roundTimer = 0;
        }

        if (Input.GetMouseButtonDown(0))
        {
            //			Debug.Log(Input.mousePosition);
            Vector3 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            mousePos.z = 0;
            Instantiate(sparkEffect, mousePos, Quaternion.identity);
        }
    }

    /*
	void OnCollisionEnter2D(Collision2D col) {
		if (col.gameObject.name == "Cans(Clone)") {
			Debug.Log(col.gameObject.name);
			Destroy (col.gameObject);
		}
	}
    */

	public void startNewRound(){
        GameObject[] cans = GameObject.FindGameObjectsWithTag("can");
        foreach (GameObject can in cans)
            Destroy(can);
        cansTimer = 0;
        roundTimer = 0;

        if (numberOfCansCreated % 5 == 0 && numberOfCansCreated > 1) {
			StartCoroutine(waitForCanNewRound(0.1f));
		}
		else{
			processForNewRound();
		}

	}
	IEnumerator waitForCanNewRound(float time){
		yield return new WaitForSeconds (time);
		processForNewRound ();
		
	}
	public void processForNewRound(){
		Constants.cons.score = 0;
		ObjHolders.sharedObjects.lblScore.text = Constants.cons.score.ToString ();

		Constants.cons.isNewRoundStarted = false;
		Constants.cons.currentRound++;
		ObjHolders.sharedObjects.lblRound.GetComponent<Text>().text = "Round : " + Constants.cons.currentRound;
//		TweenScale.Begin (ObjHolders.sharedObjects.lblRound, 0.01f, Vector3.one);
		StartCoroutine (setOFFtheObject (ObjHolders.sharedObjects.lblRound, 0.1f));
		ObjHolders.sharedObjects.lblHighScore.text = PlayerPrefs.GetInt("HighScore", 0).ToString();
	}
	IEnumerator setOFFtheObject (GameObject g, float time){
		yield return new WaitForSeconds (time);
//		TweenScale.Begin (g, time, Vector3.zero);
		Constants.cons.isNewRoundStarted = true;
	}

}
