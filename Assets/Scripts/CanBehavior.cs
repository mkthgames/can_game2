﻿using UnityEngine;
using System.Collections;

public class CanBehavior : MonoBehaviour {

    public float xSpeed;
    public float ySpeed;
    public float xDecreaseSpeedFactor;
    public float yDecreaseSpeedFactor;
    public float xMinValue;
    public float yMinValue;
    public float rotationSpeed;
    public float rotationSpeedDecreaseFactor;
    public bool isStationary = false;

    void Start()
    {
        int rnd = Random.Range(0, 4);
        //xSpeed = Random.Range(xSpeed * 1f, xSpeed * 1.45f);
        // ySpeed = Random.Range(ySpeed * 1f, ySpeed * 1.3f);
        if (this.transform.position.y == -0.7f)
        {
            switch(rnd)
            {
                case 0:
                    xSpeed = xSpeed * 0.9f;
                    ySpeed = ySpeed * 1.5f;
                    break;
                case 1:
                    xSpeed = xSpeed * 1f;
                    ySpeed = ySpeed * 1.25f;
                    break;
                case 2:
                    xSpeed = xSpeed * 1.15f;
                    ySpeed = ySpeed * 1f;
                    break;
                case 3:
                    xSpeed = xSpeed * 0.4f;
                    ySpeed = ySpeed * 1.1f;
                    break;
            }
        }
        else if (this.transform.position.y == -0.2f)
        {
            switch (rnd)
            {
                case 0:
                    xSpeed = xSpeed * 1.1f;
                    ySpeed = ySpeed * 1.05f;
                    break;
                case 1:
                    xSpeed = xSpeed * 1.2f;
                    ySpeed = ySpeed * 0.7f;
                    break;
                case 2:
                    xSpeed = xSpeed * 0.7f;
                    ySpeed = ySpeed * 0.7f;
                    break;
                case 3:
                    xSpeed = xSpeed * 0.5f;
                    ySpeed = ySpeed * 0.5f;
                    break;
            }
        }
        else if (this.transform.position.y == 0.5f)
        {
            switch (rnd)
            {
                case 0:
            xSpeed = xSpeed * 1.15f;
            ySpeed = ySpeed * 0.3f;
                    break;
                case 1:
            xSpeed = xSpeed * 1.25f;
            ySpeed = ySpeed * 0.3f;
                    break;
                case 2:
            xSpeed = xSpeed * 0.8f;
            ySpeed = ySpeed * 0.2f;
                    break;
                case 3:
            xSpeed = xSpeed * 0.6f;
            ySpeed = ySpeed * 0.1f;
                    break;
            }
        }

    }

    void Update ()
    {
        if (isStationary || Constants.cons.isGameOver || Constants.cons.isGamePaused)
            return;
        this.transform.localPosition += new Vector3 (-xSpeed, ySpeed, 0) * Time.deltaTime;
        this.transform.Rotate(new Vector3(0, 0, rotationSpeed));
        rotationSpeed -= rotationSpeedDecreaseFactor * Time.deltaTime;
        rotationSpeed = Mathf.Clamp(rotationSpeed, 5f, 100f);
        xSpeed -= xDecreaseSpeedFactor * Time.deltaTime;
        ySpeed -= yDecreaseSpeedFactor * Time.deltaTime;
        xSpeed = Mathf.Clamp(xSpeed, xMinValue, 100f);
        ySpeed = Mathf.Clamp(ySpeed, yMinValue, 100f);
    }

    public void CanPressed()
    {
        xSpeed = 0.85f;
        ySpeed = 0.75f;
        rotationSpeed = 25;
    }

    public void BecomeStationary(Transform place)
    {
        isStationary = true;
        xSpeed = 0;
        ySpeed = 0;
        rotationSpeed = 0;
        this.transform.rotation = Quaternion.Euler(Vector3.zero);
        this.transform.position = place.position;
        Destroy(this.gameObject, 0.5f);
    }

    void OnTriggerEnter2D(Collider2D coll)
    {
        if(coll.transform.tag == "10points")
        {
            if(coll.transform.position.y > 0.25f)
                GameManager._instance.ActivateLight(0);
            else
                GameManager._instance.ActivateLight(3);
            BecomeStationary(coll.transform);
            Constants.cons.score += 10;
            GameManager._instance.PlaySound("canCollected");
        }

        else if(coll.transform.tag == "20points")
        {
            GameManager._instance.ActivateLight(1);
            BecomeStationary(coll.transform);
            Constants.cons.score += 20;
            GameManager._instance.PlaySound("canCollected");
        }
        else if (coll.transform.tag == "30points")
        {
            GameManager._instance.ActivateLight(2);
            BecomeStationary(coll.transform);
            Constants.cons.score += 30;
            GameManager._instance.PlaySound("canCollected");
        }
        ObjHolders.sharedObjects.lblScore.text = Constants.cons.score.ToString();
        KeepCreatingObjects.sharedInstance.processForHighScore();
    }

    void OnCollisionEnter2D(Collision2D coll)
    {
        if(coll.transform.tag == "ground")
        {
            Constants.cons.isGameOver = true;
            GameManager._instance.gameOverScoreTxt.text = Constants.cons.score.ToString();
            ClickEvents.sharedClickEvents.showGameOverScreen();
            GameManager._instance.PlaySound("canLost");
        }
    }
}
