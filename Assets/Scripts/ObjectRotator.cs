﻿using UnityEngine;
using System.Collections;

public class ObjectRotator : MonoBehaviour {

	bool isMouseButtonDown;
	float rotationsPerMinute = 100f;
	int forceY = 80;

	bool isAbleToDetectCollision;
	// Use this for initialization
	void Start () {
				//Time.timeScale = 0.5f;
		this.gameObject.GetComponent<Rigidbody2D>().AddForce (new Vector2 (-10,forceY));
		float randYPos = Random.Range (0,2);
		float randXPos = Random.Range (-1.5f, -1f);
		GetComponent<Rigidbody2D>().velocity = new Vector2(-1.5f,1f);
		Destroy (this.gameObject, 3f);

		isAbleToDetectCollision = false;
	}
	
	// Update is called once per frame
	void FixedUpdate () {	
		if (!isAbleToDetectCollision)
			transform.Rotate(0f,0f, (float)(6.0* rotationsPerMinute * Time.fixedDeltaTime) );


	}
	void OnCollisionEnter2D(Collision2D col){
		if (isAbleToDetectCollision)
			return;
				
		transform.Rotate (0f, 0f, Mathf.Lerp (90f,0f,0.5f));
		if (col.gameObject.name != "S1(Clone)") {
			isAbleToDetectCollision = true;
			float randYPos = Random.Range (0,1);
			float randXPos = Random.Range (-1, -2);
//			rigidbody2D.velocity = new Vector2(randXPos,randYPos);
			GetComponent<Rigidbody2D>().velocity = new Vector2 (0.2f, 0.2f);
		}
		else{
			this.gameObject.GetComponent<Rigidbody2D>().AddForce (new Vector2 (-10,forceY));
			float randYPos = Random.Range (0,1);
			float randXPos = Random.Range (-1.5f, -1f);
			GetComponent<Rigidbody2D>().velocity = new Vector2(-1.5f,0.2f);
		}

		switch (col.gameObject.name) {
			case "Point50":
				Constants.cons.score+=50;
				
			break;
			case "PointDown50":
				Constants.cons.score+=50;
				
			break;
			case "Point10":
				Constants.cons.score+=10;
				
			break;
			case "Point100":
				Constants.cons.score+=100;
				
			break;
		}
		ObjHolders.sharedObjects.lblScore.text = Constants.cons.score.ToString ();	
		KeepCreatingObjects.sharedInstance.processForHighScore();


		if (col.gameObject.name == "Ground" || col.gameObject.name == "Ground1" ) {
			Constants.cons.isGameOver = true;
			ClickEvents.sharedClickEvents.showGameOverScreen();
//			return;
		}

//		this.gameObject.rigidbody2D.AddForce (new Vector2 (-2,130));




	}
	void PauseTheGame(){
		Debug.Log ("Pausing the Game");
		GetComponent<Rigidbody2D> ().isKinematic = true;
	}
	void OnEnable(){
		EventManager.OnPauseGame += PauseTheGame;
	}
	void OnDisable(){
		EventManager.OnPauseGame -= PauseTheGame;
	}
}